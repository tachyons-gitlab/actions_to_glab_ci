# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/actions_ci'

describe ActionsCi do
  it 'returns empty object' do
    expect(described_class.new.convert('')).to be_eql({})
  end

  it 'converts basic configuration' do
    actions_config = <<~YAML
      on: [push]
      jobs:
        hello:
          runs-on: ubuntu-latest
          steps:
            - run: echo "Hello World"
    YAML

    gitlab_ci_config = <<~YAML
      stages:
        - hello

      hello:
        stage: hello
        script:
          - echo "Hello World"
    YAML

    expect(described_class.new.convert(actions_config)).to match(YAML.load(gitlab_ci_config))
  end

  it 'converts a deploy script' do
    actions_config = <<~YAML
      #{'      '}
      on: [push]
      jobs:
        build:
          runs-on: ubuntu-latest
          container: golang:alpine
          steps:
            - run: apk update
            - run: go build -o bin/hello
            - uses: actions/upload-artifact@v3
              with:
                name: hello
                path: bin/hello
                retention-days: 7
        deploy:
          if: contains( github.ref, 'staging')
          runs-on: ubuntu-latest
          container: golang:alpine
          steps:
            - uses: actions/download-artifact@v3
              with:
                name: hello
            - run: echo "Deploying to Staging"
            - run: scp bin/hello remoteuser@remotehost:/remote/directory
    YAML

    gitlab_ci_config = <<~YAML

      default:
        image: golang:alpine

      stages:
        - build
        - deploy

      build:
        stage: build
        script:
          - apk update
          - go build -o bin/hello
        artifacts:
          paths:
            - bin/hello
          expire_in: 7 days

      deploy:
        stage: deploy
        script:
          - echo "Deploying to Staging"
          - scp bin/hello remoteuser@remotehost:/remote/directory
        rules:
          - if: $CI_COMMIT_BRANCH == 'staging'
    YAML

    expect(described_class.new.convert(actions_config)).to match(YAML.load(gitlab_ci_config))
  end

  it 'converts a parallel config' do
    actions_config = <<~YAML
      on: [push]
      jobs:
        python-version:
          runs-on: ubuntu-latest
          container: python:latest
          steps:
            - run: python --version
        java-version:
          if: contains( github.ref, 'staging')
          runs-on: ubuntu-latest
          container: openjdk:latest
          steps:
            - run: java -version
    YAML

    gitlab_ci_config = <<~YAML

      stages:
        - python-version
        - java-version

      python-version:
        image: python:latest
        stage: python-version
        script:
          - python --version

      java-version:
        image: openjdk:latest
        stage: java-version
        rules:
          - if: $CI_COMMIT_BRANCH == 'staging'
        script:
          - java -version
    YAML

    expect(described_class.new.convert(actions_config)).to match(YAML.load(gitlab_ci_config))
  end

  it 'converts triggers' do
    actions_config = <<~YAML
      'on':
        push:
          branches:
            - main
    YAML

    gitlab_ci_config = <<~YAML
      rules:
        - if: '$CI_COMMIT_BRANCH == main'
    YAML

    expect(described_class.new.convert(actions_config)).to match(YAML.load(gitlab_ci_config))
  end
end
