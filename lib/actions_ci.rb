# frozen_string_literal: true

require 'yaml'

class ActionsCi
  # convert GitHub action code to Gitlab CI YAML
  def convert(source)
    gitlab_yaml = {}
    source_object = YAML.load(source) || {}
    stages = source_object['jobs']&.keys || []
    jobs = source_object['jobs'] || {}
    gitlab_yaml['stages'] = stages if stages.any?

    jobs.each do |job_name, job|
      script = job['steps'].map { |step| step['run'] }.compact
      gitlab_yaml[job_name] = {
        'stage' => job_name,
        'script' => script
      }
      gitlab_yaml[job_name]['image'] = job['container'] if job.key?('container')

      if job.key?('uses') && job['uses'].start_with?('actions/cache@')
        gitlab_yaml[job_name]['cache'] = {
          'key' => job_name,
          'paths' => ['/cache'],
          'policy' => 'pull-push'
        }
      end

      # Artifact upload
      next unless job.key?('steps')

      job['steps'].each do |step|
        next unless step.key?('uses') && step['uses'].start_with?('actions/upload-artifact@')

        gitlab_yaml[job_name]['artifacts'] ||= {
          'paths' => [step['with']['path']],
          'expire_in' => "#{step['with']['retention-days']} days"
        }
      end

      # Handle if condition

      next unless job.key?('if')

      condition = translate_condition(job['if'])
      gitlab_yaml[job_name]['rules'] = [{
        'if' => condition
      }]
    end

    if jobs.count > 1
      default_keys = common_elements(gitlab_yaml.except('stages').values)

      default_keys.each do |key|
        gitlab_yaml['default'] ||= {}
        gitlab_yaml['default'][key] = gitlab_yaml.except('stages').first[1][key]

        gitlab_yaml.except('stages', 'default').each_value { |v| v.delete('image') }
      end
    end

    # handle on push branches

    source_branches = source_object.dig('on', 'push', 'branches') || []

    source_branches.each do |branch|
      gitlab_yaml['rules'] = [{
        'if' => "$CI_COMMIT_BRANCH == #{branch}"
      }]
    end

    gitlab_yaml
  end

  private

  def common_elements(hashes = [])
    return [] if hashes.empty?

    common = []
    keys = hashes.first.keys
    first_element = hashes.first

    keys.each do |key|
      common << key if hashes.all? { |hash| hash[key] == first_element[key] }
    end

    common
  end

  # translate condition contains( github.ref, 'staging') to $CI_COMMIT_BRANCH == 'staging'
  def translate_condition(condition)
    condition.gsub!(/contains\( github\.ref, '(.*)'\)/, '$CI_COMMIT_BRANCH == \'\1\'')
    condition
  end
end
